from turtle import Turtle
from ball import Ball
STYLE = ('Courier', 15, 'bold')
X_WALL = 350

class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.penup()
        self.color('White')
        self.hideturtle()
        self.l_score = 0
        self.r_score = 0
        self.goto(0, 275)

    def update_scoreboard(self):
        """ Update Scoreboard """
        self.clear()
        self.write(f"Shirts: {self.l_score}  and Skins: {self.r_score} ", font=STYLE, align='center')

    def left_score(self):
        """ award left side a point """
        self.l_score += 1
        self.update_scoreboard()

    def right_score(self):
        """ award right side a point """
        self.r_score += 1
        self.update_scoreboard()
