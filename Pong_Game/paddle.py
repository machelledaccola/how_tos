from turtle import Turtle
MOVE = 20

class Paddle(Turtle):
    def __init__(self, starting_position):
        super().__init__()
        self.shape("square")
        self.color("Pink")
        self.shapesize(stretch_wid=5, stretch_len=1)
        self.penup()
        self.goto(starting_position)

    def go_up(self):
        """ paddle move """
        new_y = self.ycor() + MOVE
        self.goto(self.xcor(), new_y)

    def go_down(self):
        """ paddle move """
        new_y = self.ycor() - MOVE
        self.goto(self.xcor(), new_y)
