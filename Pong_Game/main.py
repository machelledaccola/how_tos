from turtle import Screen
from paddle import Paddle
from ball import Ball
from scoreboard import Scoreboard
import time
MOVE = 20
Y_WALL = 280
X_WALL = 350

screen = Screen()
screen.setup(width=800, height=600)
screen.bgcolor("Black")
screen.title("PONG")
screen.tracer(0)

right_paddle = Paddle((350, 0))
left_paddle = Paddle((-350, 0))
ball = Ball()
scoreboard = Scoreboard()
scoreboard.update_scoreboard()

screen.listen()
screen.onkey(right_paddle.go_up, "Up")
screen.onkey(right_paddle.go_down, "Down")
screen.onkey(left_paddle.go_up, "W")
screen.onkey(left_paddle.go_up, "S")

is_on = True
while is_on:
    time.sleep(ball.move_speed)
    screen.update()
    ball.move()
    # Detect collision with top or bottom wall
    if abs(ball.ycor())>Y_WALL:
        ball.y_bounce()
    # Detect collision with paddle
    if ball.distance(right_paddle) <50 and ball.xcor() > 330 or ball.distance(left_paddle)<50 and ball.xcor() < -330:
        ball.x_bounce()
        #  Detect collision with left or Right wall
    if ball.xcor() > X_WALL:
        scoreboard.left_score()
        ball.reset_position()
    elif ball.xcor() < -X_WALL:
        scoreboard.right_score()
        ball.reset_position()

# TODO: set game max score and or rounds of play maybe
# TODO: figure out how to hold and release ball??
# TODO: play again????
# TODO: optional team names

screen.exitonclick()
