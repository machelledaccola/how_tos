from turtle import *
import random
turtle = Turtle()
turtle.hideturtle()
screen = Screen()
screen.setup(width=500, height=400)
screen.bgcolor("gray90")
colors = ["blue", "pink", "green", "yellow", "orange", "red"]

def bet():
    """ pop up text box with input prompt for user to pick color"""
    color_picked = False
    attempt = 0
    while not color_picked:
        user_color = screen.textinput(title="Turtle Race", prompt=f"Pick a color {colors}:")
        attempt +=1
        if user_color in colors:
            color_picked = True
            return user_color
        elif attempt == 2:
            selection = random.choice(colors)
            user_color = screen.textinput(title="Turtle Race", prompt=f"Your color is going to be: {selection}:")
            color_picked = True
            return user_color

all_turtles = []
i = 0

for shade in colors:
    ivan = Turtle(shape="turtle")
    ivan.color(shade)
    ivan.penup()
    y_axis = 175 - (i * 60)
    ivan.goto(x=(-240), y=y_axis)
    i += 1
    all_turtles.append(ivan)
user_bet = bet()
is_race_on = True

while is_race_on:
    for turtle in all_turtles:
        if turtle.xcor() >= 230:
            style = ('Courier', 10, 'bold')
            winner = turtle.pencolor()
            for turtle in all_turtles:
                ivan.hideturtle()
                ivan.goto(0, 0)
                if winner == user_bet:
                    ivan.write(f"You win, the winner was {winner}!", font=style, align='center')
                else:
                    ivan.write(f"You lose, the winner was {winner}!", font=style, align='center')
            is_race_on = False
        random_distance = random.randrange(11)
        turtle.forward(random_distance)
screen.exitonclick()

