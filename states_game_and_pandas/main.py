import turtle
import pandas

screen = turtle.Screen()
screen.title("US States Game")
image = "blank_states_img.gif"
screen.addshape(image)
turtle.shape(image)
guessed_states = []
remaining_states = []
data = pandas.read_csv("50_states.csv")
game_is_on = True

while len(guessed_states) < 50:
    answer_state = screen.textinput(title=f"{len(guessed_states)}/50 guessed right", prompt="enter a state").title()
    print(answer_state)
    all_states = data.state.to_list()
    if answer_state =="Exit":
        break
    if answer_state in all_states:
        guessed_states.append(answer_state)
        t = turtle.Turtle()
        t.hideturtle()
        t.penup()
        state_local = data[data.state == answer_state]
        t.goto(int(state_local.x), int(state_local.y))
        t.write(answer_state)

for state in all_states:
    if state not in guessed_states:
        remaining_states.append(state)

new_data = pandas.DataFrame(remaining_states)
new_data.to_csv("states_to_learn.csv")

screen.exitonclick()
# import pandas
#
# data = pandas.read_csv("./../2018data.csv")
# grey_s = len(data[data["Primary Fur Color"] == "Gray"])
# black_s = len(data[data["Primary Fur Color"] == "Black"])
# red_s = len(data[data["Primary Fur Color"] == "Cinnamon"])
# print(grey_s)
# print(red_s)
# print(black_s)
# data_dict = {
#     "fur color": ['gray', 'red', 'black'],
#     "count": [grey_s, red_s, black_s]
# }
#
# data = pandas.DataFrame(data_dict)

# print(data)

# tempuratures = []
# import pandas
# import csv
#
# data = pandas.read_csv('./../weather_data.csv')
# print(data)
# print(data["temp"])
# temp_avg = (data["temp"]).mean()
# temp_max = (data["temp"]).max()
# temp_min = (data["temp"]).min()
# print(round(temp_avg))
# print(temp_max)
# print(temp_min)
# data_dict = data.to_dict
# print(data_dict)
# temp_list = data["temp"].to_list()
# print(temp_list)
# print(round(data.temp.mean()))
# print(data.condition)
# print(data[data.day == "Monday"])
# print(data[data.temp == data.temp.max()])
# monday = data[data.day == "Monday"]
# print(round((monday.temp) *(9/5) +32))
# data_dict= {
#     'day': [data.day.to_list()],
#     'temp': [data.temp.to_list()]
# }
# new_data = pandas.DataFrame(data_dict)
# print(new_data)







