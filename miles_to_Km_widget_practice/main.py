from tkinter import *

window = Tk()
window.title('Miles to Kilometer Converter')
window.minsize(width=200, height=100)

label = Label(text='is equal to', font=("Arial", 12, "bold"))
label.grid(column=1, row=3)

label2 = Label(text= "Miles", font=("Arial", 12, "bold"))
label2.grid(column=3,row=1)

label3 = Label(text= "Km", font=("Arial", 12, "bold"))
label3.grid(column=3,row=3)

label5 = Label(text= "Miles to Kilometer Converter: ", font=("Calibri", 14, "bold"))
label5.grid(column=0, row=0)

def button_click():
    new_text = input.get()
    kilometers = round(float(new_text) * 1.609)
    label4= Label(text=f"{kilometers}", font=("Arial", 12, "bold"))
    label4.grid(column=2, row=3)

button = Button(text ="calculate", command=button_click)
button.grid(column=2,row=4)

input = Entry(width=10)
input.grid(column=2, row=1)
print(input.get())

window.mainloop()



# def add(*args):
#     print(args[1])
#     sum=0
#     for n in args:
#         sum+=n
#     return sum
#
# print(add(3, 5, 6))
#
#
# def calculate(n, **kwargs):
#     print(kwargs)
#     # for key, value in kwargs.items():
#     #    print(key)
#     #    print(item)
#     n += kwargs['add']
#     print(n)
#     n *= kwargs['multiply']
#     print(n)
#
# calculate(2, add=3, multiply=5)
#


#
#
#
#
# from tkinter import *
#
# window = Tk()
# window.title('My First GUI Program!!')
# window.minsize(width=500, height=500)
# #  Label
# label = Label(text='not a label anymore', font=("Arial", 24, "bold"))
# label.pack()
#
# #  can use either one of these to change the text in the label
# label['text'] = "NEW TExT"
# label.config(text= "NEW TExT")
# label.grid(column=0,row=0)
#
#
# #  button
#
# def button_click():
#     new_text = input.get()
#     label['text'] = f"{new_text}"
#
#
# button = Button(text ="click me", command=button_click)
# button.grid(column=1,row=1)
#
# button2 = Button(text=' no touchie me ', command=button_click)
# button2.grid(column=4, row=0)
# #Entry
#
# input = Entry(width=10)
# print(input.get())
# input.grid(column=2, row=2)
#
#
# window.mainloop()
