from turtle import Turtle
STARTING_POSITIONS = [(0, 0), (-20, 0), (-40, 0)]
MOVE_DISTANCE = 20
UP = 90
DOWN = 270
RIGHT = 0
LEFT = 180

class Snake:
    def __init__(self):
        self.segment = []
        self.create_snake()
        self.head = self.segment[0]

    def create_snake(self):
        """ starting snake creation"""
        for position in STARTING_POSITIONS:
            self.add_segment(position)

    def reset(self):
        for seg in self.segment:
            seg.goto(10000, 10000)
        self.segment.clear()
        self.create_snake()
        self.head = self.segment[0]

    def add_segment(self, position):
        new_segment = Turtle("square")
        new_segment.color("white")
        new_segment.penup()
        new_segment.goto(position)
        self.segment.append(new_segment)

    def extend(self):
        self.add_segment(self.segment[-1].position())

    def move(self):
        """ have snake segments follow the lead segment"""
        for seg_num in range(len(self.segment) - 1, 0, -1):
            """ xcor() returns the turtles xcoordinate"""
            new_x = self.segment[seg_num - 1].xcor()
            new_y = self.segment[seg_num - 1].ycor()
            self.segment[seg_num].goto(new_x, new_y)
        self.head.fd(MOVE_DISTANCE)

    def up(self):
        """ snake move up """
        if self.head.heading() != DOWN:
            self.head.setheading(UP)

    def down(self):
        """ snake move down"""
        if self.head.heading() != UP:
            self.head.setheading(DOWN)

    def right(self):
        """ snake move right """
        if self.head.heading() != LEFT:
            self.head.setheading(RIGHT)

    def left(self):
        """ snake move left """
        if self.head.heading() != RIGHT:
            self.head.setheading(LEFT)
