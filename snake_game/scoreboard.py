from turtle import Turtle
STYLE = ('Arial', 10, 'bold')

class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        with open('data.txt', mode="r") as scorefile:
            self.highscore = int(scorefile.read())
        self.hideturtle()
        self.penup()
        self.color('Black')
        self.goto(0, 270)
        self.score = 0

    def update_scoreboard(self):
        self.clear()
        self.write(f"Score: {self.score}, and High Score: {self.highscore}", font=STYLE, align='center')

    def reset(self):
        if self.score > self.highscore:
            self.highscore = self.score
            with open('data.txt', mode="w") as data:
                data.write(f"{self.highscore}")
        self.score = 0
        self.update_scoreboard()
    
    def increase_score(self):
        self.score += 1
        self.update_scoreboard()


