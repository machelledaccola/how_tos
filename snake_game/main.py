from turtle import Turtle, Screen
import time
from snake import Snake
from food import Food
from scoreboard import Scoreboard
import winsound

COLLISION = [(280, 280), ()]
STYLE = ('Arial', 10, 'bold')
WALL = 280

screen = Screen()
screen.setup(width=600, height=600)
screen.bgcolor("gray")
screen.title("Snake Game")
screen.tracer(0)

snake = Snake()
food = Food()
scoreboard = Scoreboard()
turtle = Turtle()

turtle.hideturtle()
screen.listen()
screen.onkey(snake.up, "Up")
screen.onkey(snake.down, "Down")
screen.onkey(snake.left, "Left")
screen.onkey(snake.right,"Right")

is_on = True
while is_on:
    screen.update()
    time.sleep(0.3)
    snake.move()
    #  Detect collision with food
    if snake.head.distance(food) < 15:
        winsound.PlaySound("nom_noise", winsound.SND_FILENAME)
        turtle.write("yummy nummy", font=STYLE, align='center')
        time.sleep(0.75)
        turtle.clear()
        food.refresh()
        snake.extend()
        scoreboard.increase_score()
    #  Detect collision with wall
    if abs(snake.head.xcor())>WALL or abs(snake.head.xcor())>WALL:
        winsound.PlaySound("scream", winsound.SND_FILENAME)
        scoreboard.reset()
        snake.reset()
    #  Detect collision with tail
    # if head collides with any of the segments then collision game over
    for segment in snake.segment[1:]:
        if snake.head.distance(segment) < 10:
            winsound.PlaySound("vomit", winsound.SND_FILENAME)
            scoreboard.reset()
            snake.reset()
screen.exitonclick()
