import requests
from datetime import *
import json

# you will need to fill in the spots with ????'s keys/tokens can be obtianed with free account setups
# you will need to set up a worksheet/spreadsheet on google docs with the columns from the for loop below

# here is a helpful youtube for the nutritionix api post query: https://www.youtube.com/watch?v=JRGknLE2Qc0
# be sure to like video if you watch it

# python datetime doc link: https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior

# accounts will need to be set up with google docs, sheety, and nutritionix, at time of creation they had free ones



NUTRITION_USER_ID = '??????'
password = '??????'
NUTRITION_API_KEY = '??????'
NUTRITION_APP_ID = '???????'
EXERCISE_ENDPOINT = 'https://trackapi.nutritionix.com/v2/natural/exercise'
TODAY_DATE = datetime.now().strftime("%d/%m/%y")
NOW_TIME = datetime.now().strftime("%X")
SHEETY_ENDPOINT = 'https://api.sheety.co/????????'
GENDER = '?????'
WEIGHT_KG = ????
HEIGHT_CM = ????
AGE = ????



exercise_text = input("what did you do today?: ")

exercise_headers = {
    'x-app-id': NUTRITION_APP_ID,
    'x-app-key': NUTRITION_API_KEY,
    'content-type': 'application/json',


}

user_params = {
    'query': exercise_text,
    'gender': GENDER,
    'weight_kg': WEIGHT_KG,
    'height_cm': HEIGHT_CM,
    'age': AGE,
}



nutrition_response = requests.post(EXERCISE_ENDPOINT, headers=exercise_headers, json=user_params)
result = nutrition_response.json()
print(result)

for exercise in result["exercises"]:
    sheet_inputs = {
        "workout": {
            "date": TODAY_DATE,
            "time": NOW_TIME,
            "exercise": exercise['name'].title(),
            "duration": exercise['duration_min'],
            "calories": exercise['nf_calories'],

        }
    }

    sheet_response = requests.post(SHEETY_ENDPOINT, json=sheet_inputs)

    print(sheet_response.text)

