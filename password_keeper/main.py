import tkinter
from tkinter import *
from tkinter import messagebox
from random import choice, randint, shuffle
import pyperclip


FONT = ("Courier", 9, "bold")
FONT2 = ("calibri", 7, "bold")
FONT3 = ("Courier", 8, "bold")
PINK = bg="Pink"



# ---------------------------- PASSWORD GENERATOR ------------------------------- #
def generate_password():
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
               'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
               'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

    password_list = []
    newletters = [choice(letters) for item in range(randint(8, 10))]
    newnumbers = [choice(numbers) for item in range(randint(2, 4))]
    newsymbols = [choice(symbols) for itme in range(randint(2, 4))]
    password_list = newletters + newnumbers + newsymbols
    shuffle(password_list)

    password = "".join(password_list)
    entry_password.insert(0, password)
    pyperclip.copy(password)

# ---------------------------- SAVE PASSWORD ------------------------------- #
def save_password():
    website = entry_website.get()
    email = entry_email.get()
    password = entry_password.get()

    if len(website)==0 or len(password)==0:
        messagebox.showwarning(title=None, message="Please make sure you haven't left any empty fields")
    else:
        is_ok = input(messagebox.askokcancel(title=None, message=f"Save this info?\n"
                                                             f"{website}, {email}, and password: {password}"))
        if is_ok:
            stored_info = "{}|{}|{}\n".format(email, website,password)
            with open("fake_passwords.txt", "a") as f:
                f.write(stored_info)
                entry_website.delete(0,END)
                entry_password.delete(0,END)

# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title("Password Manger")
window.config(padx=100, pady=100, bg="Pink")

canvas = Canvas(width =200, height=200, bg="Pink", highlightthickness=0)
logo_img= PhotoImage(file="logo.png")
canvas.create_image(100, 100, image=logo_img)
canvas.pack()

#Labels
label_website = Label(text="Website:", font=FONT, fg = "Black", bg="Pink")
label_website.place(x=20,y=200,anchor=E)

label_email = Label(text="Email/Username:", font=FONT, fg = "Black", bg="Pink")
label_email.place(x=20,y=220,anchor=E)

label_password = Label(text="Password:", font=FONT, fg = "Black", bg="Pink")
label_password.place(x=20,y=240,anchor=E)

entry_website = Entry(width=35)
entry_website.place(x=25,y=200,anchor=W)
entry_website.focus()

entry_email = Entry(width=35)
entry_email.place(x=25,y=220,anchor=W)
entry_email.insert(END, "fake_email@woot.fake")

entry_password = Entry(width=21)
entry_password.place(x=25,y=240,anchor=W)

button_password = Button(text="Generate",font=FONT2, bg="Pink4", fg="Black", command=generate_password)
button_password.place(x=210, y=240, anchor=E)

button_add = Button(text="Add", width=29, font=FONT3, bg="Pink4", fg="Black", command=save_password)
button_add.place(x=25, y=265, anchor=W)

quit_button = Button(text="Quit", bg="Green", fg="Red", command=window.destroy)
quit_button.place(x=0, y=0, anchor=CENTER)

window.mainloop()
