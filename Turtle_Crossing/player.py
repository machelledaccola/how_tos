from turtle import Turtle
STARTING_POSITION = (0, -280)
MOVE_DISTANCE = 10
FINISH_LINE_Y = 280

class Player(Turtle):
    def __init__(self):
        super().__init__()
        self.penup()
        self.setheading(90)
        self.shape("turtle")
        self.color("Red", "Green")
        self.shapesize(stretch_wid=1, stretch_len=1, outline=0.2)
        self.goto(STARTING_POSITION)

    def go_up(self):
        """ turtle movement up """
        self.fd(MOVE_DISTANCE)

    def reset_position(self):
        self.goto(STARTING_POSITION)


