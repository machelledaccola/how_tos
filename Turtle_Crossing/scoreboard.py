from turtle import Turtle
FONT = ("Courier", 24, "normal")

class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.penup()
        self.color("Blue3")
        self.score = 0
        self.goto(0, 260)

    def update_scoreboard(self):
        """" Update Scoreboard """
        self.clear()
        self.score += 1
        self.write(f" Score: {self.score} ", font=FONT, align='center')

    def game_over(self):
        self.clear()
        self.write(f" Game over, Final Score: {self.score}", font=FONT, align='center')

