import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard
TOP_WALL = 280

screen = Screen()
screen.setup(width=600, height=600)
screen.bgcolor("Pink")
screen.tracer(0)
player = Player()
car_manager = CarManager()
screen.listen()
screen.onkey(player.go_up, "Up")
scoreboard = Scoreboard()

game_is_on = True
while game_is_on:
    time.sleep(0.1)
    screen.update()
    car_manager.make_car()
    car_manager.car_move()
    # detect player collision with car
    for car in car_manager.all_cars:
        if car.distance(player) <20:
            game_is_on=False
            scoreboard.game_over()
    if player.ycor() > TOP_WALL:
        scoreboard.update_scoreboard()
        player.reset_position()
        car_manager.car_reset()

screen.exitonclick()
