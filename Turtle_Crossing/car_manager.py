from turtle import Turtle
import random
COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
STARTING_MOVE_DISTANCE = 5
MOVE_INCREMENT = 10

class CarManager(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.all_cars=[]
        self.move_speed = 0.1

    def make_car(self):
        """ make car and append to list of cars """
        chance = random.randint(1, 6)
        if chance == 1:
            new_car = Turtle("square")
            new_car.penup()
            new_car.color(random.choice(COLORS))
            new_car.shapesize(stretch_wid=1, stretch_len=2, outline=None)
            new_car.setheading(180)
            y_cor = random.randint(-250, 250)
            new_car.goto(300, y_cor)
            self.all_cars.append(new_car)

    def car_move(self):
        """ move cars forward """
        for car in self.all_cars:
            car.fd(STARTING_MOVE_DISTANCE)

    def car_reset(self):
        """ Reset cars, but actually speed them up """
        self.move_speed *= .09






