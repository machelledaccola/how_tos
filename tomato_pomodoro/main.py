from tkinter import *
import math
# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 20
CHECK_MARK = u'\u2713'
reps = 0
timer = None
# ---------------------------- TIMER RESET ------------------------------- # 
def reset_timer():
    global reps
    window.after_cancel(timer)
    #timer text 00:00
    canvas.itemconfig(timer_text, text="00:00")
    #title_label "Timer"
    label.config(text="Timer", font=(FONT_NAME, 35, "bold"), fg=GREEN, bg=YELLOW)
    #reset check_marks
    checks.config(text="")
    reps = 0

# ---------------------------- TIMER MECHANISM ------------------------------- # 
def start_timer():
    global reps
    reps += 1
    work_sec = WORK_MIN * 60
    short_break_sec = SHORT_BREAK_MIN * 60
    long_break_sec = LONG_BREAK_MIN * 60

    if reps == 8:
        label.config(text="Long Break", font=(FONT_NAME, 35, "bold"), fg=RED, bg=YELLOW)
        count_down(long_break_sec)
        reps=0
    elif reps % 2 == 0:
        label.config(text="Short Break", font=(FONT_NAME, 35, "bold"), fg=RED, bg=YELLOW)
        count_down(short_break_sec)

    else:
        label.config(text="Woot Work Woot", font=(FONT_NAME, 33, "bold"), fg=PINK, bg=YELLOW)
        count_down(work_sec)

# ---------------------------- COUNTDOWN MECHANISM ------------------------------- # 
def count_down(count):

    count_min = math.floor(count / 60)
    count_sec = count % 60
    if count_sec < 10:
        count_sec = f"0{count_sec}"

    canvas.itemconfig(timer_text, text=f"{count_min}:{count_sec}")
    if count > 0:
        global timer
        timer = window.after(1000, count_down, count-1)
    else:
        start_timer()
        mark = ""
        work_sessions = math.floor(reps/2)
        for _ in range(work_sessions):
            mark += CHECK_MARK
            checks.config(text=mark, font=(FONT_NAME, 20, "bold"), fg=GREEN, bg=YELLOW)
# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title("Pomodoro")
window.config(padx=100, pady=50, bg=YELLOW)

canvas = Canvas(width=200, height=300, bg=YELLOW, highlightthickness=0)
tomato_img = PhotoImage(file="tomato.png")
canvas.create_image(100, 150, image=tomato_img)
timer_text = canvas.create_text(100, 170, text="00:00", fill='white', font=(FONT_NAME, 35, "bold"))
canvas.pack()

label = Label(text="Timer",font=(FONT_NAME, 35, "bold"), fg=GREEN, bg=YELLOW)
label.place(x=100, y=00, anchor=CENTER)

checks = Label(font=(FONT_NAME, 20, "bold"), fg=GREEN, bg=YELLOW)
checks.place(x=100, y=280, anchor=CENTER)

start_button = Button(text="Start", bg=PINK,command=start_timer)
start_button.place(x=50, y=280, anchor=CENTER)

reset_button = Button(text="Reset", bg=PINK, command=reset_timer)
reset_button.place(x=150, y=280, anchor=CENTER)

quit_button = Button(text="Quit", bg=GREEN, fg=RED, command=window.destroy)
quit_button.place(x=275, y=325, anchor=CENTER)

window.mainloop()
