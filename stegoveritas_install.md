**How to install stegoveritas in kali linux:**

If you are using Kali Linux or Linux you will need to check what shell you are using. You can do this by doing the following: 

echo “$SHELL”
Or
printf “My current shell -%s\” “$SHELL”
	Please note that $SHELL is the shell for the current user only. 

If the ouput is: 
	/user/bin/bash  <br/>                  -you are using bash

If the output it 
	/user/bin/zsh	<br/>	-you are using zsh

Bash
If you are using bash the below steps are a procedure for installing stegoveritas:

1. $ pip3 install stegoveritas
	-yes go ahead and install pip3 if its not already there
1. Next make sure stegoveritas is installed in your .local bin if not find the bin it was installed in and get the absolute path to the stegoveritas file
1. export PATH=$PATH.~/foo
	-where foo is your absolute file path to stegoveritas minus the /stegoveritas… so basically ‘/home/kali/.local/bin’
1. Close your shell(s) then reopen it the $PATH routes are only loaded once upon start up so in order for changes to be applied you have to restart the machine.
1. Check it’s correct and been added by typing ‘echo $PATH’ make sure the new path you added is in there.
**Remember to restart your shell so that the new or changed $PATH search pattern will load, b/c it only loads once with the shell when you open it
1. $ stegoveritas_install_deps
1. $ stegoverias           
	-this should confirm that the application is working by printing the stegoveritas menu/options

** If you are using zsh the below steps are a procedure for installing stegoveritas:**
 
1. $ pip3 install stegoveritas
	-yes go ahead and install pip3 if its not already there
2. echo ‘export STEGOVERITAS=”$HOME/kali/.local/bin”’>>~/.zshrc
2. echo ‘export PATH=$STEGOVERITAS:$PATH”’>>~/.zshrc
2. Close your shell(s) then reopen it the $PATH routes are only loaded once upon start up so in order for changes to be applied you have to restart the machine.
2. Check it’s correct and been added by typing ‘echo $PATH’ make sure the new path you added is in there. <br/>
**Remember to restart your shell so that the new or changed $PATH search pattern will load, b/c it only loads once with the shell when you open it**
2. $ stegoveritas_install_deps
2. $ stegoverias           
	-this should confirm that the application is working by printing the stegoveritas menu/options

Resources <br/>
https://www.bleepingcomputer.com/news/linux/kali-linux-20204-switches-the-default-shell-from-bash-to-zsh/
 Kali Linux was switching from Bash to ZSH so that users could benefit from the numerous plugins, themes, and new features, including path expansions, auto directory changing, and auto-suggestions.

https://www.cyberciti.biz/tips/how-do-i-find-out-what-shell-im-using.html

https://linux.die.net/man/1/zshall



